# Example project using Debricked Pipe

This is an example project using [Debricked's Bitbucket Pipe](https://bitbucket.org/debricked/debricked-scan) to automatically 
scan repository for vulnerabilities on each commit push/pull request.